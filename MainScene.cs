using Godot;
using System;
using System.Collections.Generic;

public class MainScene : Node2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
	
	[Signal]
	public delegate void PlayerHit(int playerId);
	
	[Signal]
	public delegate void PlayerWon(int playerId);
	
	[Export]
	public PackedScene PlayerObj;
	
	private static String[] _songNames = {"DootsOut", "DootButtons", "Dooting"};
	private static Random _random = new Random();
	
	private bool multiPlayer = true;
	private bool waiting = false;
	private Texture p2umbrella;
	
	private List<Player> players = new List<Player>();
	
	
	public void StartGame(int p1Lives = 3, int p2Lives= 3)
	{
		var airCurrent = (object)GetNode<Area2D>("Area2D");
		
		var player1 = (Player)PlayerObj.Instance();
		var player2 = (Player)PlayerObj.Instance();
		player1.playerNumber = 1;
		player2.playerNumber = 2;
		player1.livesLeft = p1Lives;
		player2.livesLeft = p2Lives;
		player1.airCurrent = airCurrent;
		player2.airCurrent = airCurrent;
		player1.canBeHitBy.Add(player2.GetNode<Area2D>("arm/umbrellaArea"));
		player2.canBeHitBy.Add(player1.GetNode<Area2D>("arm/umbrellaArea"));
		player2.changeUmbrella(p2umbrella);
		player1.Connect("Hit", this, "OnPlayerHit");
		player2.Connect("Hit", this, "OnPlayerHit");
		AddChild(player1);
		AddChild(player2);
		player1.Position = GetNode<Position2D>("PlayerPosition1").Position;
		player2.Position = GetNode<Position2D>("PlayerPosition2").Position;
		players.Add(player1);
		players.Add(player2);
		foreach(Player player in players)
		{
			player.canBeHitBy.Add(GetNode<Area2D>("Floor"));
			player.canBeHitBy.Add(GetNode<Area2D>("Walls"));
			player.canBeHitBy.Add(GetNode<Area2D>("LeftWalls"));
			player.canBeHitBy.Add(GetNode<Area2D>("RightWalls"));
		}
	}
	
	public void BeginGame()
	{
		GetNode<MarginContainer>("MainMenu").Hide();
		StartGame();
	}
	
	public void EndGame()
	{
		foreach(Player player in players)
		{
			player.FreeSelf();
		}
		players = new List<Player>();
		StartGame();
	}

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        // StartGame();
		string songName = _songNames[_random.Next(0, _songNames.Length)];
		GetNode<AudioStreamPlayer>(songName).Play();
		p2umbrella = (Texture)GD.Load("res://redUmbrella.png");
    }
	
	private void AirCurrentEntered(object body)
	{
    	Player playerInCurrent = (Player)body;
		playerInCurrent.inCurrent = true;
	}
	
	private void AirCurrentExited(object body)
	{
    	Player playerOutOfCurrent = (Player)body;
		playerOutOfCurrent.inCurrent = false;
	}
//  // Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
	}
	
	public void OnPlayerHit(Player hitPlayer)
	{
		if (!hitPlayer.canDie)
		{
			return;
		}
		hitPlayer.canDie = false;
		hitPlayer.livesLeft--;
		hitPlayer.CollisionLayer = 2;
		hitPlayer.CollisionMask = 2;
		foreach(Player player in players)
		{
			if(player.canBeHitBy.Contains(hitPlayer.GetNode<Area2D>("arm/umbrellaArea")))
			{
				player.canBeHitBy.Remove(hitPlayer.GetNode<Area2D>("arm/umbrellaArea"));
			}
		}
		CallDeferred(nameof(RemoveChild), hitPlayer);
		EmitSignal(nameof(PlayerHit), hitPlayer.playerNumber);
		
		if (hitPlayer.livesLeft == 0) {
			int winnerId = hitPlayer.playerNumber % 2 + 1; // Works for 2 player mode
			EmitSignal(nameof(PlayerWon), winnerId);
		}
		else
		{
			GetNode<Timer>("GameRestartTimer").Start();
		}
	}
	
	private void GameRestartDone()
	{
		GetNode<Timer>("GameRestartTimer").Stop();
		int p1Lives = players[0].livesLeft;
		int p2Lives = players[1].livesLeft;
		foreach(Player player in players)
		{
			player.FreeSelf();
		}
		players = new List<Player>();
		StartGame(p1Lives, p2Lives);
	}
	
}
