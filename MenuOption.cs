using Godot;
using System;

public class MenuOption : Label
{
	[Signal]
	public delegate void MenuOptionPressed();
	
	// private static int LEFT = (int) KeyList.Left;
	// private static int RIGHT = (int) KeyList.Right;
	
	private bool _stillReleasing = false;
	
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        
    }

	public void _OnMenuOptionFocusEntered()
	{
	    Uppercase = true;
	}

	public void _OnMenuOptionFocusExited()
	{
	    Uppercase = false;
	}
	
	// UI Actions to support: ui_select, ui_focus_next, ui_focus_prev
	public override void _GuiInput(InputEvent ev)
	{
        if (ev is InputEventAction ac)
		{
			string action = ac.Action;
            bool pressed = ac.Pressed;

            // var num = action[action.Length-1];
			
            if (pressed && (action == "ui_left1" && Input.IsActionPressed("ui_right1")
                        || action == "ui_left2" && Input.IsActionPressed("ui_right2")
                        || action == "ui_right1" && Input.IsActionPressed("ui_left1")
                        || action == "ui_right2" && Input.IsActionPressed("ui_left2")))
            {
                if (!_stillReleasing)
                {
                    EmitSignal(nameof(MenuOptionPressed));
                }
            }
            else if (!pressed) // released
            {
                if (action != "ui_left" && action.StartsWith("ui_left"))
                {
                    var num = action[action.Length-1];
                    if (Input.IsActionPressed(String.Format("ui_right{0}", num)))
                    {
                        _stillReleasing = true;
                    }
                    else if (_stillReleasing)
                    {
                        _stillReleasing = false;
                    }
                    else
                    {
                        AcceptEvent();
						var newAction = new InputEventAction();
						newAction.Action = "ui_left";
						newAction.Pressed = true;
						Input.ParseInputEvent(newAction);
                    }
                }
                else if (action != "ui_right" && action.StartsWith("ui_right"))
                {
                    var num = action[action.Length-1];
                    if (Input.IsActionPressed(String.Format("ui_left{0}", num)))
                    {
                        _stillReleasing = true;
                    }
                    else if (_stillReleasing)
                    {
                        _stillReleasing = false;
                    }
                    else
                    {
                        AcceptEvent();
						var newAction = new InputEventAction();
						newAction.Action = "ui_left";
						newAction.Pressed = true;
						Input.ParseInputEvent(newAction);
                    }
                }
			}
		}
	}
}






//using Godot;
//using System;
//
//public class MenuOption : Label
//{
//	private static int LEFT = (int)KeyList.Left;
//	private static int RIGHT = (int)KeyList.Right;
//
//	public bool RightPressed { get; set; }
//	public bool LeftPressed { get; set; }
//	public bool SelectPressed { get; set; }
//
//    // Called when the node enters the scene tree for the first time.
//    public override void _Ready()
//    {
//        GrabFocus(); // MenuOption should be auto-selected
//    }
//
//	public void _OnMenuOptionFocusEntered()
//	{
//	    Uppercase = true;
//	}
//
//	public void _OnMenuOptionFocusExited()
//	{
//	    Uppercase = false;
//	}
//
//	// UI Actions to support: ui_select, ui_focus_next, ui_focus_prev
//	public override void _GuiInput(InputEvent ev)
//	{
////		if ((ev.IsAction("ui_focus_next") || ev.IsAction("ui_focus_prev")) && ev.IsPressed())
////		{
////			// AcceptEvent(); // The key was only pressed; ignore until released
////		}
//		if (ev is InputEventKey keyEv)
//		{
//			var key = keyEv.Scancode;
//			var pressed = keyEv.Pressed;
//
//			if (pressed && (key == LEFT && Input.IsKeyPressed(RIGHT) || key == RIGHT && Input.IsKeyPressed(LEFT)))
//			{
//				// create ui_select
//				// Create an action
//				AcceptEvent();
//				var action = new InputEventAction();
//				action.Action = "ui_select";
//				action.Pressed = true;
//				Input.ParseInputEvent(action);
//			}
//			else if (!pressed)
//			{
//				if (key == LEFT)
//				{
//					AcceptEvent();
//					// create ui_left
//					var action = new InputEventAction();
//					action.Action = "ui_left";
//					action.Pressed = true;
//					Input.ParseInputEvent(action);
//
//				}
//				else if (key == RIGHT)
//				{
//					AcceptEvent();
//					// create ui_right
//					var action = new InputEventAction();
//					action.Action = "ui_right";
//					action.Pressed = true;
//					Input.ParseInputEvent(action);
//				}
//			}
//		}
//	}
//
//	/*
//    // UI Actions to support: ui_select, ui_focus_next, ui_focus_prev
//	public override void _GuiInput(InputEvent ev)
//	{
////		if ((ev.IsAction("ui_focus_next") || ev.IsAction("ui_focus_prev")) && ev.IsPressed())
////		{
////			// AcceptEvent(); // The key was only pressed; ignore until released
////		}
//		if (ev is InputEventKey keyEv)
//		{
//			if (keyEv.Scancode == (int)KeyList.Left || keyEv.Scancode == (int)KeyList.Right)
//			{
//				// Consume the event whether it was pressed or not
//				if (!keyEv.Pressed)
//				{
//					// Create an action
//					var action = new InputEventAction();
//					action.Action = keyEv.Scancode == (int)KeyList.Left ? "ui_focus_prev" : "ui_focus_next";
//					action.Pressed = true;
//					Input.ParseInputEvent(action);
//				}
//
//				AcceptEvent();
//			}
//		}
//	}
//	*/
//
//
////  // Called every frame. 'delta' is the elapsed time since the previous frame.
////  public override void _Process(float delta)
////  {
////      
////  }
//}
