using Godot;
using System;

public class HUD : MarginContainer
{
	[Signal]
	public delegate void GameOver();
	
	private static string VICTORY_MSG_FMT = "Player {0} Wins!!!";
	private static int NUM_LIVES = 3;
	
    private int _numPlayer1Lives = NUM_LIVES;
	private int _numPlayer2Lives = NUM_LIVES;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        // GetNode<Label>("VBoxContainer/WinnerLabel").Hide();
    }
	
	public void ResetHUD()
	{
		GetNode<Label>("VBoxContainer/WinnerLabel").Hide();
		_numPlayer1Lives = NUM_LIVES;
		_numPlayer2Lives = NUM_LIVES;
		
		for (int i = 1; i <= 2; i++)
		{
			for (int j = 1; j <= NUM_LIVES; j++)
			{
				string name = String.Format("VBoxContainer/Lives/Player{0}Lives/Heart{1}", i, j);
				GetNode<TextureRect>(name).Visible = true;
			}
		}
	}
	
	public void _OnPlayerHit(int playerId)
	{
		int numLives = 0;
		if (playerId == 1)
		{
			numLives = --_numPlayer1Lives;
		}
		else
		{
			numLives = --_numPlayer2Lives;
		}
		LoseLife(playerId, numLives);
	}
	
	/*
	public void _OnPlayer1Hit()
	{
		_numPlayer1Lives--;
		LoseLife(1, _numPlayer1Lives);
	}
	
	public void _OnPlayer2Hit()
	{
		_numPlayer2Lives--;
		LoseLife(2, _numPlayer2Lives);
	}
	*/
	
	private void LoseLife(int playerId, int numLives)
	{
		var name = String.Format("VBoxContainer/Lives/Player{0}Lives/Heart{1}", playerId, numLives+1);
		if(!HasNode(name))
		{
			return;
		}
		GetNode<TextureRect>(name).Visible = false;
		
		/*
		if (numLives == 0)
		{
			DisplayVictoryMessage(GetWinnerId(playerId));
		}
		*/
	}
	
	private int GetWinnerId(int loserId)
	{
		return loserId % 2 + 1;
	}
	
	private void _OnGameWon(int winnerId)
	{
		var winnerLabel = GetNode<Label>("VBoxContainer/WinnerLabel");
		winnerLabel.Text = String.Format(VICTORY_MSG_FMT, winnerId);
		winnerLabel.Visible = true;
		GetNode<Timer>("VictoryMsgTimer").Start();
	}

	public void _OnVictoryMsgTimerTimeout()
	{
		GetNode<Timer>("VictoryMsgTimer").Stop();
		ResetHUD();
		EmitSignal(nameof(GameOver));
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
