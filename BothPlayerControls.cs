using Godot;
using System;

public class BothPlayerControls : Label
{
	[Signal]
	public delegate void StartGame();
	
	private static int LEFT = (int) KeyList.Left;
	private static int RIGHT = (int) KeyList.Right;
	
	private static int A = (int) KeyList.A;
	private static int D = (int) KeyList.D;
	
	// Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        GrabFocus();
    }
	
	public override void _GuiInput(InputEvent ev)
	{
		if (ev is InputEventKey keyEv)
		{
			int key = keyEv.Scancode;
			bool pressed = keyEv.Pressed;
			
			if (pressed && ((key == LEFT && Input.IsKeyPressed(RIGHT))
							|| (key == RIGHT && Input.IsKeyPressed(LEFT))
							|| (key == A && Input.IsKeyPressed(D))
							|| (key == D && Input.IsKeyPressed(A))))
			{
				ReleaseFocus();
				EmitSignal(nameof(StartGame));
			}
		}

//		if (ev is InputEventAction ac)
//		{
//			string action = ac.Action;
//			bool pressed = ac.Pressed;
//
//			if (pressed && ((action == "ui_left1" && Input.IsActionPressed("ui_right1"))
//						|| (action == "ui_left2" && Input.IsActionPressed("ui_right2"))
//						|| (action == "ui_right1" && Input.IsActionPressed("ui_left1"))
//						|| (action == "ui_right2" && Input.IsActionPressed("ui_left2"))))
//			{
//				EmitSignal(nameof(StartGame));
//			}
//			else
//			{
//				// EmitSignal(nameof(StartGame));
//			}
//		}
//		else
//		{
//			// EmitSignal(nameof(StartGame));
//		}
	}
	
	/*
	private void _on_MainMenu_StartGame()
	{
	    var label = GetNode<Label>("MenuItems/MusicCredits");
		label.Hide();
	}
	*/

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
