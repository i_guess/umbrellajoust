using Godot;
using System;

public class MainMenu : MarginContainer
{
	[Signal]
	public delegate void StartGame();
	
	// Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        // GrabFocus();
    }

	private void _OnBothPlayerControlsStartGame()
	{
	    EmitSignal(nameof(StartGame));
	}

	/*
	public override void _GuiInput(InputEvent ev)
	{
		if (ev is InputEventAction ac)
		{
			string action = ac.Action;
			bool pressed = ac.Pressed;
			
			if (pressed && ((action == "ui_left1" && Input.IsActionPressed("ui_right1"))
						|| (action == "ui_left2" && Input.IsActionPressed("ui_right2"))
						|| (action == "ui_right1" && Input.IsActionPressed("ui_left1"))
						|| (action == "ui_right2" && Input.IsActionPressed("ui_left2"))))
			{
				// EmitSignal(nameof(StartGame));
			}
			else
			{
				EmitSignal(nameof(StartGame));
			}
		}
		else
		{
			// EmitSignal(nameof(StartGame));
		}
	}
	*/

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}

