using Godot;
using System;
using System.Collections.Generic;

public class Player : RigidBody2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
	
	public bool inCurrent = false;
	
	[Signal]
	public delegate void Hit(Player hitPlayer);
	
	[Export]
	public float airSpeed;
	
	public int playerNumber;
	
	public int livesLeft = 3;
	
	public object airCurrent;
	
	public bool canDie = true;
	
	private static Random perLegRandom = new Random();
	
	public List<object> canBeHitBy = new List<object>();

	public void changeUmbrella(Texture umbrella)
	{
		GetNode<Sprite>("umbrella").Texture = umbrella;
	}

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
    }
	
	public override void _PhysicsProcess(float delta)
	{
		float rotationAddition = 0f;
		if(Input.IsActionPressed("ui_right"+this.playerNumber))
		{
			rotationAddition += delta*2f;
		}
		if(Input.IsActionPressed("ui_left"+this.playerNumber))
		{
			rotationAddition -= delta*2f;
		}
		Sprite arm = GetNode<Sprite>("arm");
		
		arm.Rotation += rotationAddition;
		GetNode<Sprite>("umbrella").Rotation += rotationAddition;
		
		float calcRotation = (arm.Rotation + Mathf.Pi/2) % (Mathf.Pi*2);

		
		if(inCurrent && calcRotation < Mathf.Pi && (calcRotation > 0 || calcRotation < -Mathf.Pi))
		{
			// Basic air current is moving up
			Vector2 currentDirection = new Vector2(0f, -1f).Rotated(arm.Rotation);
			ApplyCentralImpulse(currentDirection*airSpeed*delta);
			GetNode<Sprite>("leg").Rotation += rotationAddition;

		}
		else
		{

		}
		
		float leg1Rot = GetNode<Sprite>("leg").Rotation;
		rotationAddition = -leg1Rot * delta;
		GetNode<Sprite>("leg").Rotation += rotationAddition;
		
		if(Rotation != 0f)
		{
			Rotation += -Rotation*delta;
		}
	}
	private void OnBodyAreaEntered(object body)
	{
		
		if(canBeHitBy.Contains(body))
		{

			Hide();
			EmitSignal("Hit", this);
		}
	}
	
	private void UmbrellaHit(object area)
	{

	}
	
	private void ImmunityTimerDone()
	{
		
	}
	
	public void FreeSelf()
	{
		QueueFree();
	}


}











